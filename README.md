Welcome to Neon Breakout
by Ivan Kotchouro
kotchouro.i@gmail.com
-----------------------------------
Project started March 2015
Unity Project; most recent version can be found at
http://mamuto.fomac.net/breakout_web/neon_breakout_0_5/neon_breakout_0_5.html
older:
http://mamuto.fomac.net/breakout_web/archive/breakout_web_offline.html
-----------------------------------

05/24/15 03:51
version 0.5
1. Score/Life/Level tracker implemented but is not displaying in web.
2. End Screen/Logic added.
3. 3 levels created for testing purposes.
4. Spent a lot of time debugging web player as loading levels was an issue.
5. ^hackish solution using coroutines.

05/19/15 18:32
* Initial commit after a few months of sporadic development.
* Level editor works, basic game works.
* Need work on level picker, score/life/level tracker, UI, end screen/logic.
* Also need to make actual levels.
___________________________________